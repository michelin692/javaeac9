package persistencia;

import components.Dissenyador;
import components.Jardiner;
import components.Torn;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import principal.Estudi;
import principal.GestorEstudisException;
import principal.Projecte;

/**
 *
 * @author cesca
 */
public class GestorXML {
    private Document doc;
    private Estudi estudi;

    public Estudi getEstudi() {
        return estudi;
    }

    public void setEstudi(Estudi estudi) {
        this.estudi = estudi;
    }

    public void desarEstudi(String nomFitxer, Estudi estudi) throws GestorEstudisException, ParserConfigurationException {
        construeixModel(estudi);
        desarModel(nomFitxer);
    }

    public void carregarEstudi(String nomFitxer) throws GestorEstudisException {
        carregarFitxer(nomFitxer);
        fitxerEstudi();
    }

    /*Paràmetres: Estudi a partir de la qual volem construir el model
     *
     *Acció: 
     * - Llegir els atributs de l'objecte estudi passat per paràmetre 
     *   per construir un model (document XML) sobre el Document doc (atribut de GestorXML).
     *
     * - L'arrel del document XML és "estudi". Aquesta arrel, l'heu d'afegir 
     *   a doc. Un cop fet això, heu de recórrer l'ArrayList components d'estudi
     *   i per a cada component, afegir un fill a doc. Cada fill 
     *   tindrà com atributs els atributs de l'objecte (nif, nom, …).
     *   Si l'atribut a afegir és l'atribut booleà actiu o finalitzat, quan aquests siguin verdaders
     *   el valor de l'atribut del document XML serà 1, en cas contrari 0.
     *
     * - Si es tracta d'un jardiner, a més, heu d'afegir un fill addicional amb 
     *   el seu torn.
     *
     * - Si es tracte d'un projecte, a més, heu d'afegir fills addicionals amb 
     *   els dissenyador i jardiners assignats.
     *
     *Retorn: cap
     */
    public void construeixModel(Estudi estudi) throws GestorEstudisException, ParserConfigurationException {
        
        try{
        
            //Creació del document
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            doc = builder.newDocument();

            ///Element arrel
            Element arrel = doc.createElement("Estudi");
            arrel.setAttribute("adreca",estudi.getAdreca());
            arrel.setAttribute("codi",String.valueOf(estudi.getCodi()));
            arrel.setAttribute("nom",estudi.getNom());




            //recorrer el array list
            for(int i=0;i<estudi.getListaComponentes().size();i++){

                if(estudi.getListaComponentes().get(i) instanceof Dissenyador){
                    //Elements fill de l'anterior
                    Element dissenyador = doc.createElement("dissenyador");
                    if(((Dissenyador) estudi.getListaComponentes().get(i)).getActiu()){
                        dissenyador.setAttribute("actiu","1");
                    }else{
                        dissenyador.setAttribute("actiu","0");
                    }
                    dissenyador.setAttribute("nif",((Dissenyador) estudi.getListaComponentes().get(i)).getNif());
                    dissenyador.setAttribute("nom",((Dissenyador) estudi.getListaComponentes().get(i)).getNom());
                    arrel.appendChild(dissenyador);
                }else if(estudi.getListaComponentes().get(i) instanceof Jardiner){
                    //Elements fill de l'anterior
                    Element jardiner = doc.createElement("jardiner");
                    if(((Jardiner) estudi.getListaComponentes().get(i)).getActiu()){
                        jardiner.setAttribute("actiu","1");
                    }else{
                        jardiner.setAttribute("actiu","0");
                    }
                    jardiner.setAttribute("nif",((Jardiner) estudi.getListaComponentes().get(i)).getNif());
                    jardiner.setAttribute("nom",((Jardiner) estudi.getListaComponentes().get(i)).getNom());

                    //si tenemos turno lo añadimos al XML.
                    if( ((Jardiner) estudi.getListaComponentes().get(i)).getTorn() != null ){
                        Element torn = doc.createElement("torn");
                        torn.setAttribute("codi",((Jardiner) estudi.getListaComponentes().get(i)).getTorn().getCodi());
                        torn.setAttribute("horaAcabament",((Jardiner) estudi.getListaComponentes().get(i)).getTorn().getHoraAcabament());
                        torn.setAttribute("horaInici",((Jardiner) estudi.getListaComponentes().get(i)).getTorn().getHoraInici());
                        torn.setAttribute("nom",((Jardiner) estudi.getListaComponentes().get(i)).getTorn().getNom());
                        jardiner.appendChild(torn);
                    }


                    arrel.appendChild(jardiner);

                }else if(estudi.getListaComponentes().get(i) instanceof Projecte){

                    Element projecte = doc.createElement("projecte");
                    projecte.setAttribute("codi",String.valueOf(((Projecte) estudi.getListaComponentes().get(i)).getCodi()));
                    if( ((Projecte) estudi.getListaComponentes().get(i)).isFinalitzat() ){
                        projecte.setAttribute("finalitzat","1");
                    }else{
                        projecte.setAttribute("finalitzat","0");
                    }
                    projecte.setAttribute("nifClient",((Projecte) estudi.getListaComponentes().get(i)).getNifClient());
                    projecte.setAttribute("pressupost",String.valueOf(((Projecte) estudi.getListaComponentes().get(i)).getPressupost()));


                    //sizeMapTreballedor = ((Projecte) estudi.getListaComponentes().get(i)).getMapTreballedor().size();
                    
                    ((Projecte) estudi.getListaComponentes().get(i)).getMapTreballedor().forEach((clave,valor)->{
                        if( valor instanceof Jardiner ){
                            Element jardiner = doc.createElement("jardiner");
                            if(valor.getActiu()){
                                jardiner.setAttribute("actiu","1");
                            }else{
                                jardiner.setAttribute("actiu","0");
                            }
                            jardiner.setAttribute("nif",valor.getNif());
                            jardiner.setAttribute("nom",valor.getNom());

                             //si tenemos turno lo añadimos al XML.
                            if( ((Jardiner) valor).getTorn() != null ){
                                Element torn = doc.createElement("torn");
                                torn.setAttribute("codi",((Jardiner) valor).getTorn().getCodi());
                                torn.setAttribute("horaAcabament",((Jardiner) valor).getTorn().getHoraAcabament());
                                torn.setAttribute("horaInici",((Jardiner) valor).getTorn().getHoraInici());
                                torn.setAttribute("nom",((Jardiner) valor).getTorn().getNom());
                                jardiner.appendChild(torn);
                            }


                            projecte.appendChild(jardiner);
                        }else if( valor instanceof Dissenyador ){
                            Element dissenyador = doc.createElement("dissenyador");
                            if(valor.getActiu()){
                                dissenyador.setAttribute("actiu","1");
                            }else{
                                dissenyador.setAttribute("actiu","0");
                            }
                            dissenyador.setAttribute("nif",valor.getNif());
                            dissenyador.setAttribute("nom",valor.getNom());
                            projecte.appendChild(dissenyador);
                        }
                    });

                    arrel.appendChild(projecte);
                    
                }else if(estudi.getListaComponentes().get(i) instanceof Torn){
                    Element turno = doc.createElement("torn");
                    turno.setAttribute("codi",((Torn) estudi.getListaComponentes().get(i)).getCodi());
                    turno.setAttribute("horaAcabament",((Torn) estudi.getListaComponentes().get(i)).getHoraAcabament());
                    turno.setAttribute("horaInici",((Torn) estudi.getListaComponentes().get(i)).getHoraInici());
                    turno.setAttribute("nom",((Torn) estudi.getListaComponentes().get(i)).getNom());
                    arrel.appendChild(turno);
                }
            }
            doc.appendChild(arrel);
        
        }catch (Exception ex) {
            throw new GestorEstudisException("GestorXML.model");
        }
       
    }

    public void desarModel(String nomFitxer) throws GestorEstudisException {
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            DOMSource source = new DOMSource(doc);
            File f = new File(nomFitxer + ".xml");
            StreamResult result = new StreamResult(f);
            transformer.transform(source, result);
        } catch (Exception ex) {
            throw new GestorEstudisException("GestorXML.desar");
        }
    }

    public void carregarFitxer(String nomFitxer) throws GestorEstudisException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            File f = new File(nomFitxer + ".xml");
            doc = builder.parse(f);
        } catch (Exception ex) {
            throw new GestorEstudisException("GestorXML.carrega");
        }
    }
    
    /*Paràmetres: cap
     *
     *Acció: 
     * - Llegir el fitxer del vostre sistema i carregar-lo sobre l'atribut doc, 
     *   per assignar valors als atributs d'estudi i la resta d'objectes que formen 
     *   els components d'estudi.
     *    
     * - Primer heu de crear l'objecte estudi a partir de l'arrel del document XML
     *   per després recórrer el doc i per cada fill, afegir un objecte a l'atribut 
     *   components d'estudi mitjançant el mètode escaient de la classe Estudi.
    
     * - Si l'element del document XML que s'ha llegit és un jardiner, recordeu 
     *   que a més d'afegir-lo a components, també haureu d'assignarli el torn 
     *   corresponent.
     *
     * - Si l'element del document XML que s'ha llegit és un projecte, recordeu 
     *   que a més d'afegir-lo a components, també haureu d'assignar-li
     *   el dissenyador i jardiners corresponents.
     *
     * - Penseu que els estats actius dels treballadors i finalitzats del projecte,
     *   s'han d'afegir un cop creat l'objecte pertinent.
     *
     *Retorn: cap
     */
    private void fitxerEstudi() throws GestorEstudisException {
        
        try{
            
            Element documentXML = doc.getDocumentElement();
            String adreca = documentXML.getAttribute("adreca");
            String codi = documentXML.getAttribute("codi");
            String nom = documentXML.getAttribute("nom");
            //creamos el estudio
            estudi = new Estudi(Integer.parseInt(codi),nom,adreca);
            //obtenemos todos los hijos del elemento padre.
            NodeList nodeList = documentXML.getChildNodes();
            //recorremos el documento para saber el nombre del nodo de cada elemento e ir entrando en cada caso.
            for (int i = 0; i < nodeList.getLength(); i++) {

                Element elem = (Element)nodeList.item(i);
                
                if (elem.getNodeType() == Node.ELEMENT_NODE && elem.getNodeName() == "dissenyador" ) {
                    
                    String activo = elem.getAttribute("actiu");
                    String nif = elem.getAttribute("nif");
                    String nombre = elem.getAttribute("nom");

                    estudi.addDissenyador(new Dissenyador(nif,nombre));
                
                }else if( elem.getNodeType() == Node.ELEMENT_NODE && elem.getNodeName() == "jardiner" ){
                    
                    String activo = elem.getAttribute("actiu");
                    String nif = elem.getAttribute("nif");
                    String nombre = elem.getAttribute("nom");

                    estudi.addJardiner(new Jardiner(nif,nombre));
                
                }else if( elem.getNodeType() == Node.ELEMENT_NODE && elem.getNodeName() == "projecte" ){
                    
                    String codigo = elem.getAttribute("codi");
                    String finalitzat = elem.getAttribute("finalitzat");
                    String nifClient = elem.getAttribute("nifClient");
                    String pressupost = elem.getAttribute("pressupost");
                    //añadimos el projecto al estudio
                    estudi.addProjecte(new Projecte(Integer.parseInt(codigo),nifClient,Double.parseDouble(pressupost)));
                    //accedemos a los hijos del projecte.
                    NodeList nodeListProjectes = elem.getChildNodes();
                    //recorremos los elementos hijos del projecte.
                    for (int p = 0; p < nodeListProjectes.getLength(); p++) {
                        
                        Element elemtoDentroProjecto = (Element)nodeListProjectes.item(p);
                        
                        if (elemtoDentroProjecto.getNodeType() == Node.ELEMENT_NODE && elemtoDentroProjecto.getNodeName() == "dissenyador" ) {
                            
                            String activo = elemtoDentroProjecto.getAttribute("actiu");
                            String nif = elemtoDentroProjecto.getAttribute("nif");
                            String nombre = elemtoDentroProjecto.getAttribute("nom");
                            
                            estudi.getListaComponentes().add(new Dissenyador(nif,nombre));
                            
                        }else if( elemtoDentroProjecto.getNodeType() == Node.ELEMENT_NODE && elemtoDentroProjecto.getNodeName() == "jardiner" ){
                            
                            String activo = elemtoDentroProjecto.getAttribute("actiu");
                            String nif = elemtoDentroProjecto.getAttribute("nif");
                            String nombre = elemtoDentroProjecto.getAttribute("nom");
                            
                            estudi.getListaComponentes().add(new Jardiner(nif,nombre));
                            
                            //comprobamos si tiene un turno el jardinero.
                            //accedemos a los hijos del jardinero.
                            NodeList nodeListJardineroDentroProyecto = elemtoDentroProjecto.getChildNodes();
                            
                            for (int s = 0; s < nodeListJardineroDentroProyecto.getLength(); s++) {
                                
                                Element eleTornDentroJardinero = (Element)nodeListJardineroDentroProyecto.item(s);
                                
                                if( eleTornDentroJardinero.getNodeType() == Node.ELEMENT_NODE && elemtoDentroProjecto.getNodeName() == "torn" ){
                                    String codigoTurno = eleTornDentroJardinero.getAttribute("codi");
                                    String HoraIniciTurn = eleTornDentroJardinero.getAttribute("horaInici");
                                    String HoraFiTurn = eleTornDentroJardinero.getAttribute("horaAcabament");
                                    String NomTurn = eleTornDentroJardinero.getAttribute("nom");
                                    //me falta por añadir este turno al jardinero.
                                    estudi.addTornJardiner();
                                }
                            }
                            
                        }
                    }
                    
                }else if( elem.getNodeType() == Node.ELEMENT_NODE && elem.getNodeName() == "torn" ){
                    
                    String codiTorn = elem.getAttribute("codi");
                    String horaInici = elem.getAttribute("horaInici");
                    String horaAcabament = elem.getAttribute("horaAcabament");
                    String nombreTorn = elem.getAttribute("nom");

                    estudi.addTorn(new Torn(codiTorn,nombreTorn,horaInici,horaAcabament));
                }

            }
            
        } catch (Exception ex) {
            throw new GestorEstudisException("lecutaDocument");
        }
        
        
        
        
        
        
        
        
                
    }
}
